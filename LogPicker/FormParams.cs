﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LogPicker
{
    public partial class FormParams : Form
    {
        Form1 Parent;
        public FormParams(Form1 parent)
        {
            InitializeComponent();
            this.Parent = parent;
        }

        private void FormParams_Load(object sender, EventArgs e)
        {

        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            List<string> listLines = new List<string>();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = openFileDialog1.FileName;
            }
        }

        private void btnPick_Click(object sender, EventArgs e)
        {
            this.Close();
            Parent.Pick(txtPath.Text, txtKeys.Lines, chbAnd.Checked);
        }
    }
}
