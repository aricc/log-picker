﻿using System;
using System.IO;
using System.Windows.Forms;

namespace LogPicker
{
    public partial class Form1 : Form
    {
        private FormParams formParams;
        public Form1()
        {
            InitializeComponent();

            formParams = new FormParams(this);
        }

        private void 打开OToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formParams.ShowDialog();
        }

        public void Pick(string path, string[] keys, bool and = true)
        {
            string[] lines = File.ReadAllLines(path);
            tspbProcess.Maximum = lines.Length;
            tspbProcess.Value = 0;
            tsslStatus.Text = "提取中...";
            Application.DoEvents();
            if (lines != null && lines.Length > 0)
            {
                txtPickResult.Text = "";
                Application.DoEvents();

                if (and)
                {
                    foreach (string line in lines)
                    {

                        bool matchAllKeys = true;
                        foreach (string key in keys)
                        {
                            if (!line.Contains(key))
                            {
                                matchAllKeys = false;
                                break;
                            }
                        }

                        tspbProcess.Value++;
                        Application.DoEvents();

                        if (!matchAllKeys) continue;

                        txtPickResult.AppendText(line + "\r\n");
                    }
                }
                else
                {
                    foreach (string line in lines)
                    {
                        bool matchKey = false;
                        foreach (string key in keys)
                        {
                            if (line.Contains(key))
                            {
                                matchKey = true;
                                break;
                            }
                        }

                        tspbProcess.Value++;
                        Application.DoEvents();

                        if (!matchKey) continue;

                        txtPickResult.AppendText(line + "\r\n");
                    }
                }
            }
            else
            {
                MessageBox.Show("您选择的文件是空的，请检查您的日志文件是否正确。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            tsslStatus.Text = "提取完毕";
        }
    }
}
